#!/usr/bin/env -S just --justfile

# Naive check if /Users or {{HOME}} for home.
S := if "${HOME}" =~ '/U.*' {
		"macos"
	} else {
		if `cat /etc/issue 2>/dev/null || true` =~ "Deb.*" { "debian" } else { "ubuntu" }
	}
PWD := `pwd`
HOME := "${HOME}"
USER := "${USER}"

import? 'justs/linux.just'
import? 'justs/macos.just'
import? 'justs/python.just'
import? 'justs/rust.just'
import? 'justs/symlinks.just'

list-vars:
  echo "{{PWD}}"
  echo "{{HOME}}"
  echo "{{USER}}"

git-add:
	git add .

ssh-keys:
    cat home/ssh/*.pub > ~/.ssh/authorized_keys

