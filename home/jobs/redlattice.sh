#!/bin/bash
function portaldsave() {
  docker save portald:devel | gzip > portal.tar.gz
}


function rl() {
    for var in "$@"; do
        open -a iTerm ~/rl/$var
        settitle $var
    done
}


function rl-gsub() {
  git submodule update --init --recursive --remote
}

function rl-portalshell() {
  docker compose -f /home/tsouza/rl/vail-dev/docker-compose.yml exec portald bash
}
function rl-portalrunshell() {
  docker compose -f /home/tsouza/rl/vail-dev/docker-compose.yml run --rm -it portald bash
}
function rl-dbshell() {
  docker compose -f /home/tsouza/rl/vail-dev/docker-compose.yml exec portal-db bash
}
function rl-syncnode() {
  docker compose -f /home/tsouza/rl/vail-dev/docker-compose.yml exec portald portal-manage sync_node
}
function rl-redisshell() {
  docker compose -f /home/tsouza/rl/vail-dev/docker-compose.yml exec portal-redis bash
}

function rl-bash() {
  docker compose -f /home/tsouza/rl/vail-dev/docker-compose.yml exec $1 bash
}
function rl-runbash() {
  docker compose -f /home/tsouza/rl/vail-dev/docker-compose.yml run --rm -it $1 bash
}
