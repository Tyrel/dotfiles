#!/bin/bash

# Check if filename and line number are provided
if [ $# -ne 2 ]; then
    echo "Usage: $0 <filename> <line_number>"
    exit 1
fi

# Extract filename and line number from arguments
filename=$1
line_number=$2

# Get the commit hash where the line was last changed
commit_hash=$(git blame -L "$line_number,$line_number" -- "$filename" | awk '{print $1}')

# Get the email address of the committer
email_address=$(git show --no-patch --format='%ae' $commit_hash)

echo "Email address of the committer: $email_address"

